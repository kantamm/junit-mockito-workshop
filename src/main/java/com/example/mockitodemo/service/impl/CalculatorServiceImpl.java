package com.example.mockitodemo.service.impl;

import com.example.mockitodemo.service.CalculatorService;
import com.example.mockitodemo.service.ExternalMultiplyService;
import org.springframework.stereotype.Service;

@Service
public class CalculatorServiceImpl implements CalculatorService {
    private ExternalMultiplyService externalMultiplyService;

    public CalculatorServiceImpl(ExternalMultiplyService externalMultiplyService) {
        this.externalMultiplyService = externalMultiplyService;
    }

    public String multiply(Integer a, Integer b) {
        if (a == null || b == null) {
            throw new RuntimeException("Input must not be null");
        }
        Integer result = externalMultiplyService.multiply(a, b);
        return String.format("%d x %d = %d", a, b, result);
    }
}