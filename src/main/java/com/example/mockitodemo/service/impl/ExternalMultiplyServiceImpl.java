package com.example.mockitodemo.service.impl;

import com.example.mockitodemo.service.CalculatorService;
import com.example.mockitodemo.service.ExternalMultiplyService;
import org.springframework.stereotype.Service;

@Service
public class ExternalMultiplyServiceImpl implements ExternalMultiplyService {
    @Override
    public Integer multiply(Integer a, Integer b) {
        return a*b;
    }
}