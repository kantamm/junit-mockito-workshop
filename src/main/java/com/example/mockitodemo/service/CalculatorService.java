package com.example.mockitodemo.service;

public interface CalculatorService {
    public String multiply(Integer a, Integer b);
}