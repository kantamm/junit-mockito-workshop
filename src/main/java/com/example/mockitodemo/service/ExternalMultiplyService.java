package com.example.mockitodemo.service;

public interface ExternalMultiplyService {
    public Integer multiply(Integer a, Integer b);
}