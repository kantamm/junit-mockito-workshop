package com.example.mockitodemo;

import com.example.mockitodemo.service.CalculatorService;
import com.example.mockitodemo.service.ExternalMultiplyService;
import com.example.mockitodemo.service.impl.CalculatorServiceImpl;
import com.example.mockitodemo.service.impl.ExternalMultiplyServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
class CalculatorServiceTest {
	@MockBean
	ExternalMultiplyService externalMultiplyService;

	@Autowired
	CalculatorService calculatorService;

	@Test
	public void shouldCalculateSuccessfullyWhenMultiplyServiceReturnCorrectValue_JUnit() {
		// arrange
		ExternalMultiplyService externalMultiplyService1 = new ExternalMultiplyServiceImpl();
		CalculatorService calculatorService1 = new CalculatorServiceImpl(externalMultiplyService1);

		// actual
		String actual = calculatorService1.multiply(3,4);

		// assert
		assertEquals("3 x 4 = 12", actual);
	}

	@Test
	public void shouldCalculateSuccessfullyWhenMultiplyServiceReturnCorrectValue_JUnitStub() {
		// arrange
		ExternalMultiplyService externalMultiplyService1 = new ExternalMultiplyServiceStub();
		CalculatorService calculatorService1 = new CalculatorServiceImpl(externalMultiplyService1);

		// actual
		String actual = calculatorService1.multiply(3,4);

		// assert
		assertEquals("3 x 4 = 12", actual);
	}

	@Test
	public void shouldCalculateSuccessfullyWhenMultiplyServiceReturnCorrectValue() {
		// arrange
		when(externalMultiplyService.multiply(3,4)).thenReturn(12);

		// actual
		String actual = calculatorService.multiply(3,4);

		// assert
		assertEquals("3 x 4 = 12", actual);
	}

	@Test
	public void shouldThrowExceptionIfInputIsNull() {
		// arrange
		when(externalMultiplyService.multiply(3,4)).thenReturn(12);

		// actual
		Throwable throwable = assertThrows(RuntimeException.class, () -> {
			String actual = calculatorService.multiply(null,4);
		});

		// assert
		assertEquals("Input must not be null", throwable.getMessage());
	}

}