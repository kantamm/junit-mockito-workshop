package com.example.mockitodemo;

import com.example.mockitodemo.service.ExternalMultiplyService;

public class ExternalMultiplyServiceStub implements ExternalMultiplyService {
    @Override
    public Integer multiply(Integer a, Integer b) {
        return 12;
    }
}
